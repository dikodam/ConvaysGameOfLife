package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import spiel.Spielfeld;

public class GUI {
    private JFrame frame;
    private Spielfeld spielfeld;
    private int groesse;
    private JMenuBar menuBar;
    private JMenu menuSpiel;
    private int generation;

    public static void main(String[] args) {
	new GUI();
    }

    public GUI() {
	frame = getFrame();
	initFrame();

	frame.setJMenuBar(getMenu());
	frame.add(getNorth(), BorderLayout.NORTH);
	frame.add(getCenter(), BorderLayout.CENTER);
	frame.add(getSouth(), BorderLayout.SOUTH);

	frame.setVisible(true);
    }

    private JMenuBar getMenu() {
	if (menuBar == null) {
	    menuBar = new JMenuBar();
	    menuBar.add(getMenuSpiel());
	}
	return menuBar;
    }

    private JMenu getMenuSpiel() {
	if (menuSpiel == null) {
	    menuSpiel = new JMenu("Spiel");
	    JMenuItem miNeu = new JMenuItem("Neu...");
	    menuSpiel.add(miNeu);
	    miNeu.addActionListener(e -> {
		neuesSpiel();
		updateCenter();
	    });
	}
	return menuSpiel;
    }

    /**
     * Ruft einen "Neues Spiel"-Dialog auf, der den Client die Spielfeldgröße
     * bestimmen lässt
     */
    private void neuesSpiel() {
	String eingabe = JOptionPane.showInputDialog(null, "Bitte geben Sie die Spielfeldgröße ein: ", "Neues Spiel",
		JOptionPane.PLAIN_MESSAGE);
	Integer spielfeldgroesse;
	try {
	    spielfeldgroesse = Integer.parseInt(eingabe);
	} catch (NumberFormatException e) {
	    JOptionPane.showMessageDialog(frame, "Ungültige Zahl eingegeben, es wird der Defaultwert 5 benutzt!",
		    "Fehleingabe", JOptionPane.WARNING_MESSAGE);
	    spielfeldgroesse = 5;
	}
	generation = 0;

	initSpielfeld(spielfeldgroesse);

	updateFrame();
    }

    // private JDialog getDialog(JFrame parent, String name, boolean modal) {
    // JDialog dialog = new JDialog(parent, name, modal);
    // dialog.setSize(400, 300);
    // dialog.setLocationRelativeTo(parent);
    // // TODO anderes Layout bzw. das centerPanel schöner machen
    // dialog.setLayout(new BorderLayout());
    //
    // JLabel lInfo = new JLabel("Bitte die Spielfeldgröße eingeben: ");
    // JTextField tfGroesse = new JTextField(8);
    //
    // JPanel centerPanel = new JPanel();
    // centerPanel.add(lInfo);
    // centerPanel.add(tfGroesse);
    //
    // JButton bOK = new JButton("OK");
    // bOK.addActionListener(e -> {
    // // TODO idk irgendwie muss der inhalt von tf übergeben werden
    // });
    // JButton bAbbrechen = new JButton("Abbrechen");
    //
    // dialog.add(tfGroesse, BorderLayout.CENTER);
    // dialog.add(bOK, BorderLayout.SOUTH);
    // dialog.add(bAbbrechen, BorderLayout.SOUTH);
    //
    // return dialog;
    // }

    /**
     * Initialisiert ein neues quadratisches Spielfeld (groesse * groesse)
     * 
     * @param groesse
     *            Gibt die Zeilenbreite an
     */
    private void initSpielfeld(int groesse) {
	this.groesse = groesse;
	spielfeld = new Spielfeld(groesse);
	spielfeld.printSpielfeld();
    }

    private JFrame getFrame() {
	if (frame == null) {
	    frame = new JFrame();
	}
	return frame;
    }

    /**
     * Frameinitialisation mit DefaultCloseOperaton, Size, LocationRelative,
     * BorderLayout
     */
    private void initFrame() {
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(700, 600);
	frame.setLocationRelativeTo(null);
	frame.setLayout(new BorderLayout());
    }

    private Component getNorth() {
	JPanel pNorth = new JPanel();

	JButton bNext = new JButton("nächste Generation");
	bNext.addActionListener(e -> {
	    if (spielfeld != null) {
		spielfeld.berechneNeueGeneration();
		generation++;
		updateCenter();
		updateSouth();
	    }
	});
	pNorth.add(bNext);

	return pNorth;
    }

    private void updateFrame() {
	frame.remove(frame.getContentPane());
	updateNorth();
	updateCenter();
	updateSouth();
    }

    private void updateNorth() {
	frame.add(getNorth(), BorderLayout.NORTH);
	frame.validate();
    }

    private void updateCenter() {

	frame.add(getCenter(), BorderLayout.CENTER);
	frame.validate();
    }

    private void updateSouth() {
	frame.add(getSouth(), BorderLayout.SOUTH);
	frame.validate();

    }

    private Component getCenter() {
	JPanel pCenter = new JPanel();

	if (spielfeld != null) {
	    pCenter.setLayout(new GridBagLayout());
	    GridBagConstraints gbc;
	    for (int x = 0; x < (groesse); x++) {
		for (int y = 0; y < (groesse); y++) {
		    gbc = new GridBagConstraints();
		    gbc.gridx = x;
		    gbc.gridy = y;

		    if (spielfeld.lebtZelle(x, y)) {
			pCenter.add(new JButton("O"), gbc);
		    } else {
			pCenter.add(new JButton("X"), gbc);
		    }
		}
	    }
	}

	return pCenter;
    }

    private Component getSouth() {
	JPanel pSouth = new JPanel();

	JLabel lAwesomeAdam = new JLabel("Generation: " + generation);
	pSouth.add(lAwesomeAdam);

	return pSouth;
    }
}
