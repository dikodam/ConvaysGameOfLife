package test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import spiel.Spielfeld;
import spiel.Zelle;

public class SpielfeldTest {

    private static final int SPIELFELDGROESSE = 4;
    Spielfeld spielfeld;
    Zelle[][] zellen;

    @Before
    public void setUp() {
	spielfeld = new Spielfeld(SPIELFELDGROESSE);
	zellen = spielfeld.getSpielfeld();
    }

    @Test
    public void testSpielfeldInit() {
	// im Konstruktor gecallt
	assertThat(zellen.length, is(SPIELFELDGROESSE));
	for (Zelle[] zelle : zellen) {
	    assertThat(zellen.length, is(SPIELFELDGROESSE));
	}
    }

    @Test
    public void testNachbarnErzeugenFuer() {
	// im Konstruktor gecallt
	// TODO eckfelder übergreifend abfragen
	Zelle zelle00 = zellen[0][0];
	assertThat(zelle00.getNachbar(0), notNullValue());
    }

    @Test
    public void testBerechneNeueGeneration() {

	testNachbarnErzeugenFuer();
    }

    @Test
    public void testPrintSpielfeld() {
	fail("Not yet implemented");
    }

}
