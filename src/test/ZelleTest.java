package test;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;

import spiel.Zelle;

public class ZelleTest {

    Zelle zelle;

    @Before
    public void setUp() {
	zelle = new Zelle();
    }

    @Test
    public void testIsLebendigFalse() {
	assertThat(zelle.istLebendig(), is(false));
    }

    @Test
    public void testIsLebendigTrue() {
	zelle.setLebenig(true);
	assertThat(zelle.istLebendig(), is(true));
    }

    @Test
    public void testGetX() {
	assertThat(zelle.getX(), is(0));
    }

    @Test
    public void testGetY() {
	assertThat(zelle.getY(), is(0));
    }

    @Test
    public void testSetGetNachbar() {
	Zelle nachbarzelle = new Zelle();
	zelle.setNachbar(1, nachbarzelle);
	assertThat(zelle.getNachbar(1), is(sameInstance(nachbarzelle)));
	// sugar; sameInstance() ohne is() w�rde gen�gen
	Zelle nachbarzelle2 = new Zelle();
	zelle.setNachbar(7, nachbarzelle2);
	assertThat(zelle.getNachbar(7), is(sameInstance(nachbarzelle2)));
    }

    @Test
    public void testSetPosition() {
	zelle.setPosition(2, 3);
	assertThat(zelle.getX(), is(2));
	assertThat(zelle.getY(), is(3));
    }

    @Test
    public void testToString() {
	zelle.setPosition(1, 2);
	zelle.setLebenig(true);
	assertThat(zelle.toString(), is("<Zelle 1:2, lebendig> "));

	Zelle zelle2 = new Zelle();
	zelle2.setPosition(3, 4);
	zelle2.setLebenig(false);
	assertThat(zelle2.toString(), is("<Zelle 3:4, tot     > "));
    }

    @Test
    public void testRegel1() {

	Zelle lebendingerNachbar = new Zelle(true);
	Zelle toterNachbar = new Zelle(false);

	Zelle regel1 = new Zelle();

	for (int i = 0; i < 8; i++) {
	    regel1.setNachbar(i, toterNachbar);
	}

	regel1.setNachbar(0, lebendingerNachbar);
	// 1) Jede lebende Zelle, die weniger als 2 lebende Nachbarzellen hat,
	// stirbt
	// <2 lebendige Nachbarn -> tot

	assertThat(regel1.regelnAnwenden(), is(false));
    }

    @Test
    public void testRegel2() {

	// 2) Jede lebende Zelle, die 2 oder 3 lebendige Nachbarzellen hat,
	// lebt
	// weiter

	// (=2 ODER =3)lebendige Nachbarn UND lebendig -> lebt

	Zelle lebendingerNachbar = new Zelle(true);
	Zelle toterNachbar = new Zelle(false);

	Zelle regel2lebendig2lebendeNachbarn = new Zelle(true);
	Zelle regel2lebendig3lebendeNachbarn = new Zelle(true);
	Zelle regel2tot2lebendeNachbarn = new Zelle(false);
	Zelle regel2tot3lebendeNachbarn = new Zelle(false);

	for (int i = 0; i < 8; i++) {
	    regel2lebendig2lebendeNachbarn.setNachbar(i, toterNachbar);
	    regel2lebendig3lebendeNachbarn.setNachbar(i, toterNachbar);
	    regel2tot2lebendeNachbarn.setNachbar(i, toterNachbar);
	    regel2tot3lebendeNachbarn.setNachbar(i, toterNachbar);
	}

	regel2lebendig2lebendeNachbarn.setNachbar(0, lebendingerNachbar);
	regel2lebendig2lebendeNachbarn.setNachbar(1, lebendingerNachbar);

	regel2lebendig3lebendeNachbarn.setNachbar(0, lebendingerNachbar);
	regel2lebendig3lebendeNachbarn.setNachbar(1, lebendingerNachbar);
	regel2lebendig3lebendeNachbarn.setNachbar(2, lebendingerNachbar);

	regel2tot2lebendeNachbarn.setNachbar(0, lebendingerNachbar);
	regel2tot2lebendeNachbarn.setNachbar(1, lebendingerNachbar);

	regel2tot3lebendeNachbarn.setNachbar(0, lebendingerNachbar);
	regel2tot3lebendeNachbarn.setNachbar(1, lebendingerNachbar);
	regel2tot3lebendeNachbarn.setNachbar(2, lebendingerNachbar);

	assertThat(regel2lebendig2lebendeNachbarn.regelnAnwenden(), is(true));
	assertThat(regel2lebendig3lebendeNachbarn.regelnAnwenden(), is(true));
	// widerspricht REGEL 4
	// assertThat(regel2tot2lebendeNachbarn.regelnAnwenden(), is(false));
	assertThat(regel2tot3lebendeNachbarn.regelnAnwenden(), is(false));
    }

    @Test
    public void testRegel3() {

	Zelle lebendingerNachbar = new Zelle(true);
	Zelle toterNachbar = new Zelle(false);

	Zelle regel3 = new Zelle();

	// TODO Test für jede Regel

	// 3) Jede lebende Zelle, die mehr als 3 lebende Nachbarzellen hat,
	// stirbt

	// >3 lebendige Nachbarn -> tot
	assertThat(regel3.regelnAnwenden(), is(false));
    }

    @Test
    public void testRegel4() {

	Zelle lebendingerNachbar = new Zelle(true);
	Zelle toterNachbar = new Zelle(false);

	Zelle regel4 = new Zelle();

	// TODO Test für jede Regel

	// 4) Jede tote Zelle, die genau 3 lebende Nachbarzellen hat, wird
	// lebendig

	// tot UND =3 lebendige Nachbarn -> lebt
	assertThat(regel4.regelnAnwenden(), is(true));
    }

}
