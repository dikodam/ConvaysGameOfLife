package spiel;

import java.util.Random;

public class Spielfeld {

    private Zelle[][] spielfeld;
    private int zeilenbreite;

    public Spielfeld(int zeilenbreite) {
	spielfeld = new Zelle[zeilenbreite][zeilenbreite];
	this.zeilenbreite = zeilenbreite;
	spielfeldInit();
    }

    private void spielfeldInit() {
	zellenErzeugen();
	graphErzeugen();
    }

    private void zellenErzeugen() {
	System.out.println("Zellenerzeugung...");
	for (int x = 0; x < spielfeld.length; x++) {
	    for (int y = 0; y < spielfeld[0].length; y++) {
		boolean leben = new Random().nextBoolean();
		spielfeld[x][y] = new Zelle(leben);
		spielfeld[x][y].setPosition(x, y);
		System.out.println(spielfeld[x][y]);
	    }
	}
    }

    private void graphErzeugen() {
	System.out.println("Grapherzeugung...");
	for (Zelle[] zellenzeile : spielfeld) {
	    for (Zelle zelle : zellenzeile) {
		nachbarnErzeugenFuer(zelle);
	    }
	}
    }

    private void nachbarnErzeugenFuer(Zelle zelle) {

	int zellenX = zelle.getX();
	int zellenY = zelle.getY();

	int position = 0;

	for (int xIterator = zellenX - 1; xIterator < zellenX + 2; xIterator++) {
	    for (int yIterator = zellenY - 1; yIterator < zellenY + 2; yIterator++) {
		int xPos = xIterator;
		int yPos = yIterator;

		if (xIterator == -1) {
		    xPos = zeilenbreite - 1;
		} else if (xIterator == zeilenbreite) {
		    xPos = 0;
		}

		if (yIterator == -1) {
		    yPos = zeilenbreite - 1;
		} else if (yIterator == zeilenbreite) {
		    yPos = 0;
		}

		if (zellenX != xIterator && zellenY != yIterator) {
		    zelle.setNachbar(position, spielfeld[xPos][yPos]);
		    position++;
		}

	    }
	}

    }

    public void berechneNeueGeneration() {
	Zelle[][] neuesSpielfeld = new Zelle[zeilenbreite][zeilenbreite];
	for (Zelle[] zellenzeile : spielfeld) {
	    for (Zelle zelle : zellenzeile) {
		Zelle neueZelle = new Zelle(zelle.regelnAnwenden());
		neuesSpielfeld[zelle.getX()][zelle.getY()] = neueZelle;
		// TODO (zellen in neuem Zelle[][] speichern) und am Ende
		// spielfeld �berschreiben
		// Graph neu erzeugen
	    }
	}
    }

    public void printSpielfeld() {
	System.out.println("Spielfeldausgabe: ");
	for (Zelle[] zellenzeile : spielfeld) {
	    for (Zelle zelle : zellenzeile) {
		System.out.print(zelle);
	    }
	    System.out.println();
	}
    }

    public Zelle[][] getSpielfeld() {
	return spielfeld;
    }

    public int getGroesse() {
	return zeilenbreite;
    }

    public boolean lebtZelle(int x, int y) {
	return spielfeld[x][y].istLebendig();
    }
}
