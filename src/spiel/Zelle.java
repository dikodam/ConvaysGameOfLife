package spiel;

public class Zelle {

    private boolean lebendig;
    private Zelle[] nachbarzelle;
    private int x;
    private int y;

    public Zelle(boolean lebendig) {
	setLebenig(lebendig);
	nachbarzelle = new Zelle[8];
    }

    public Zelle() {
	this(false);
    }

    public boolean istLebendig() {
	return lebendig;
    }

    public void setLebenig(boolean leben) {
	this.lebendig = leben;
    }

    public Zelle getNachbar(int position) {
	if (position < 0 || position > 7) {
	    throw new IllegalArgumentException(
		    "Es k�nnen nur Nachbarn mit Position zwischen 0 und 7 angesprochen werden!");
	}
	return nachbarzelle[position];
    }

    /**
     * Setzt die mitgegebene Zelle als Nachbar an mitgegebener Position.
     * 
     * @param position
     *            Position zwischen 0 und 7, an der die Zelle gespeichert wird,
     *            beginnend mit links oben. Nummerierung von rechts nach links,
     *            von oben nach unten.
     * @param nachbarzelle
     *            zu setzende Zelle
     */
    public void setNachbar(int position, Zelle nachbarzelle) {
	if (position < 0 || position > 7) {
	    throw new IllegalArgumentException("Argument " + position + " ung�ltig!"
		    + " Es k�nnen nur Nachbarn mit Position zwischen 0 und 7 angesprochen werden!");
	}
	this.nachbarzelle[position] = nachbarzelle;
    }

    public void setPosition(int x, int y) {
	// TODO lazy initialization
	this.x = x;
	this.y = y;
    }

    public int getX() {
	return x;
    }

    public int getY() {
	return y;
    }

    @Override
    public String toString() {
	if (lebendig) {
	    return "<Zelle " + x + ":" + y + ", lebendig> ";
	}
	return "<Zelle " + x + ":" + y + ", tot     > ";
    }

    public boolean regelnAnwenden() {
	// Anzahl lebender Nachbar ermitteln
	int lebendeNachbarn = 0;
	for (int i = 0; i < nachbarzelle.length; i++) {
	    if (nachbarzelle[i].istLebendig()) {
		lebendeNachbarn++;
	    }
	}

	// 1) Jede lebende Zelle, die weniger als 2 lebende Nachbarzellen hat,
	// stirbt

	// <2 lebendige Nachbarn -> tot

	// 2) Jede lebende Zelle, die 2 oder 3 lebendige Nachbarzellen hat, lebt
	// weiter

	// (=2 ODER =3)lebendige Nachbarn UND lebendig -> lebt

	// 3) Jede lebende Zelle, die mehr als 3 lebende Nachbarzellen hat,
	// stirbt

	// >3 lebendige Nachbarn -> tot

	// 4) Jede tote Zelle, die genau 3 lebende Nachbarzellen hat, wird
	// lebendig

	// tot UND =3 lebendige Nachbarn -> lebt

	switch (lebendeNachbarn) {
	case 0:
	case 1:
	    return false;
	case 2:
	    return this.lebendig;
	case 3:
	    return true;
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	default:
	    return false;
	}

    }
}
